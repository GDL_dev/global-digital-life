TIMEZONE = 'utc'

POSTGRES = {
    "database": "database",
    "user": "user",
    "password": "password"
}

MEDIA_DIR = './media'

# Touch-reload
AUTO_START = True
BEFORE_RELOAD_SLEEP = 2
CHECK_TIMEOUT = 5
