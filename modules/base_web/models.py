import string

from aiohttp import web
from playhouse.postgres_ext import *
from validate_email import validate_email as _validate_email

from core.database import BaseModel, CITextField
from core.json import Deserializable
from . import utils

ALLOWED_USERNAME_SYMBOLS = string.ascii_letters + string.digits + '_.'


def validate_email(email):
    if not _validate_email(email):
        raise web.HTTPBadRequest(reason='Email address is invalid!')
    if len(User.select().where(User.email == email).execute()):
        raise web.HTTPBadRequest(reason='User with this email address is already registered!')
    return True


def validate_password(password):
    length = len(password)
    if length < 8:
        raise web.HTTPBadRequest(reason='Password is too short!')
    elif length > 32:
        raise web.HTTPBadRequest(reason='Password is too long!')
    return True


class User(BaseModel, Deserializable):
    public_fields = ['~uid', 'email', 'first_name', 'last_name']

    email = CITextField(unique=True)
    password = TextField()

    first_name = CharField(max_length=128, default=None, null=True)
    last_name = CharField(max_length=128, default=None, null=True)

    uid = UUIDField(unique=True, default=uuid.uuid4)
    token = CharField(max_length=128)

    @property
    def full_name(self):
        name = ''
        if self.first_name:
            name += self.first_name
            if self.last_name:
                name += ' ' + self.last_name
        else:
            name = self.email
        return name

    @classmethod
    def register(cls, email, password, first_name=None, last_name=None):
        """
        Register new user with username and password

        :param email:
        :param password:
        :param first_name:
        :param last_name:
        :return:
        """
        validate_email(email)
        validate_password(password)

        uid = uuid.uuid4()
        hashed_password = utils.hash_password(password)
        token = utils.generate_token(uid)

        return User.create(uid=uid, email=email, password=hashed_password, token=token,
                           first_name=first_name, last_name=last_name)

    @classmethod
    def authenticate(cls, email, password) -> 'User':
        try:
            user = cls.get(User.email == email)
        except User.DoesNotExist:
            raise web.HTTPBadRequest(reason='This email address is not registered!')
        else:
            if not utils.check_password(password, user.password):
                raise web.HTTPBadRequest(reason='Password is invalid!')
            return user

    @classmethod
    def authenticate_token(cls, token) -> 'User':
        try:
            user = cls.get(User.token == token)
        except User.DoesNotExist:
            raise web.HTTPUnauthorized(reason='Bad token!')
        else:
            return user

    def deserialize_uid(self):
        return str(self.uid)

    # def deserialize(self, *, include=None, exclude=None):
    #     data = super(User, self).deserialize(include=include, exclude=exclude)

    #     if 'uid' in data:
    #         data['uid'] = str(data['uid'])

    #     return data

    def __str__(self):
        return '@{0}({1})'.format(self.uid, self.email)
