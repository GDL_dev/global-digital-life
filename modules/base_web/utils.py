import secrets

from passlib.hash import pbkdf2_sha256


def hash_password(password):
    return pbkdf2_sha256.using(rounds=8000, salt_size=10).hash(password)


def check_password(password, hash_):
    return pbkdf2_sha256.verify(password, hash_)


def generate_token(uuid):
    return str(uuid) + ':' + secrets.token_hex(45)
