import asyncio

from aiohttp import web

from core import json
from .models import User


class APIController(web.View):
    login_required = True

    @property
    def request(self) -> web.Request:
        return self._request

    async def get_token(self) -> str:
        """
        Detect auth method and return token.

        Cookies: 'auth'
        Headers: 'X-Auth'

        :return:
        """
        if 'auth' in self.request.cookies:
            token = self.request.cookies.get('auth')
        elif 'X-Auth' in self.request.headers:
            token = self.request.headers.get('X-Auth')
        else:
            raise web.HTTPUnauthorized()
        return token

    async def get_user(self) -> User:
        if not hasattr(self, '_user'):
            setattr(self, '_user', User.authenticate_token(await self.get_token()))
        return getattr(self, '_user')

    @asyncio.coroutine
    def __iter__(self):
        if self.login_required:
            yield from self.get_user()

        data = yield from super(APIController, self).__iter__()
        if not isinstance(data, web.StreamResponse):
            if isinstance(data, json.Deserializable):
                data = data.deserialize()

            response = {'ok': True, 'result': data}

            return web.json_response(response, dumps=json.dumps)
        return data
