import core
from . import controllers


async def on_startup(app):
    app.router.add_route('*', '/', controllers.HomePageController, name='home')
    app.router.add_route('*', '/user', controllers.CurrentUserController, name='user')
    app.router.add_route('*', '/user/login', controllers.LoginController, name='login')
    app.router.add_route('*', '/user/register', controllers.RegisterController, name='register')
    app.router.add_route('*', '/user/test', controllers.TestController, name='test')


core.app.on_startup.append(on_startup)
