import os

from aiohttp import web

import core
from .models import User
from .web import APIController


class HomePageController(APIController):
    login_required = False

    async def get(self):
        # return {'version': core.__version__}
        return web.FileResponse(os.path.join(core.BASE_DIR, 'static', 'reg.html'))


class CurrentUserController(APIController):
    login_required = False

    async def get(self):
        try:
            user = await self.get_user()
        except web.HTTPException:
            return 'Unauthorized'
        else:
            return user.deserialize()


class LoginController(APIController):
    login_required = False

    async def post(self):
        data = await self.request.post()

        if 'email' in data and 'password' and data:
            user = User.authenticate(data.get('email'), data.get('password'))
        elif 'token' in data:
            user = User.authenticate_token(data.get('token'))
        else:
            raise web.HTTPBadRequest(reason='Token or email with password is required!')

        return web.json_response(user.deserialize(include=['token']), headers={
            'Set_cookie': 'auth=' + user.token
        })


class RegisterController(APIController):
    login_required = False

    async def post(self):
        data = await self.request.post()

        if data.get('password1', '') != data.get('password2', ''):
            raise web.HTTPBadRequest(reason='Passwords don\'t match!')

        user = User.register(
            email=data.get('email', ''),
            password=data.get('password1', ''),
            first_name=data.get('first_name'),
            last_name=data.get('last_name'),
        )

        return web.json_response(user.deserialize(include=['token']), headers={
            'Set_cookie': 'auth=' + user.token
        })


class TestController(APIController):
    login_required = True

    async def get(self):
        return {'number': 42, 'bool': True, 'None': None,
                'user': (await self.get_user()).deserialize()}
