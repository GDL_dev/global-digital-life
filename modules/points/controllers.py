from aiohttp import web

from core import json
from modules.base_web.web import APIController
from modules.points.models import Point


class AddPointController(APIController):
    async def post(self):
        user = await self.get_user()

        data = await self.request.post()

        raw_colors = data.get('colors', [])
        try:
            colors = json.loads(raw_colors)
            if not isinstance(colors, list):
                raise TypeError()
        except (ValueError, TypeError) as e:
            raise web.HTTPBadRequest(reason='Colors list must be an valid JSON list!')

        if any(color not in Point.VALID_COLOR_NAMES for color in colors):
            raise web.HTTPBadRequest(reason='Colors list has bad color name!\n'
                                            'Allowed names: "' + '", "'.join(Point.VALID_COLOR_NAMES) + '"')

        point = Point(user=user, colors=colors)
        point.save()

        result = point.deserialize()
        return result
