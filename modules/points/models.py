from playhouse.postgres_ext import *

from core.database import BaseModel
from core.json import Deserializable
from modules.base_web.models import User


class Point(BaseModel, Deserializable):
    public_fields = ['~user', 'colors']

    VALID_COLOR_NAMES = ['red', 'yellow', 'orange', 'green', 'blue', 'dark_blue', 'violet']

    user = ForeignKeyField(User)

    colors = JSONField()

    def deserialize_user(self):
        return str(self.user.uid)
