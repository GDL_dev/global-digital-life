import core
from . import controllers


async def on_startup(app):
    app.router.add_route('*', '/points/add', controllers.AddPointController, name='write')


core.app.on_startup.append(on_startup)
