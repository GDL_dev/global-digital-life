import core
from . import controllers


async def on_startup(app):
    app.router.add_route('*', '/media/upload', controllers.UploadsController, name='upload')


core.app.on_startup.append(on_startup)
