import datetime
import os
import secrets
import urllib.parse

from playhouse.postgres_ext import *

import settings
from core.database import BaseModel, CITextField
from core.json import Deserializable
from modules.base_web.models import User
from modules.points.models import Point


class Upload(BaseModel, Deserializable):
    public_fields = ['~user', 'file_name', 'size', 'sha1', 'color', '~uploaded']
    # File has owner
    user = ForeignKeyField(User)

    # File info
    file_name = CITextField()
    size = IntegerField()
    sha1 = CharField()
    color = CITextField()

    # Path in FS
    path = TextField()

    # Statistic
    downloads = IntegerField(default=0)

    # Upload identity
    identity = UUIDField(default=uuid.uuid4)

    deleted = BooleanField(default=False)

    def deserialize_user(self):
        return str(self.user.uid)

    def deserialize_uploaded(self):
        return self.created_date.isoformat()

    @classmethod
    def generate_path(cls, filename: str) -> str:
        """
        Returns filename with 16 symbols hex code

        /path/to/file.tar.gz -> /path/to/file_aabbccddeeff1122.tar.gz

        :param filename:
        :return:
        """
        path, only_filename = os.path.split(filename)
        raw_filename, _, ext = only_filename.partition('.')

        new_filename = raw_filename + '_' + secrets.token_hex(8) + '.' + ext
        return os.path.join(path, new_filename)

    def inc_downloads(self):
        Upload.update(downloads=Upload.downloads + 1,
                      modified_date=datetime.datetime.now()).where(Upload.identity == self.identity).execute()

    @classmethod
    def get_upload(cls, user=None, identity=None, filename=None):
        query = [
            (Upload.deleted == False)
        ]
        if user:
            query.append((Upload.user == user))
        if identity:
            query.append((Upload.identity == identity))
        if filename:
            filename = urllib.parse.unquote(filename)
            query.append((Upload.file_name == filename))
        return Upload.get(*query)

    def delete_file(self, fs=True, auto_save=False):
        if fs:
            os.remove(self.fs_path)
        self.deleted = True
        if auto_save:
            self.save()

    @property
    def fs_path(self):
        return os.path.join(settings.MEDIA_DIR, self.path)

    @classmethod
    def detect_color(cls, date: datetime.datetime):
        return Point.VALID_COLOR_NAMES[date.isoweekday() - 1]
