import datetime
import hashlib
import os

from aiohttp import MultipartReader, BodyPartReader

import settings
from modules.base_web.models import User
from modules.base_web.web import APIController
from modules.uploads.models import Upload


class UploadsController(APIController):
    async def post(self):
        user = await self.get_user()
        reader: MultipartReader = await self.request.multipart()

        result = {}
        while True:
            file: BodyPartReader = await reader.next()
            if file is None:
                break

            # Get result
            field = file.name
            if not file.filename:
                # Field is not file
                result[field] = None
            else:
                # Field is file
                result[field] = await self.upload(user, file)

        return result

    async def upload(self, user: User, reader: BodyPartReader):
        # Generate names and path's
        now = datetime.datetime.now()
        upload_filename = Upload.generate_path(reader.filename)
        upload_path = os.path.join(*map(str, (user.uid, now.year, now.month, now.day)))
        file_path = os.path.join(upload_path, upload_filename)
        full_path = os.path.join(settings.MEDIA_DIR, upload_path)

        # make dirs
        os.makedirs(full_path, exist_ok=True)

        # Init DB record
        upload = Upload(user=user, file_name=reader.filename, path=file_path)

        # Read file from multipart data
        size = 0
        sha1 = hashlib.sha1()

        with open(os.path.join(full_path, upload_filename), 'wb') as f:
            while True:
                chunk = await reader.read_chunk()  # 8192 bytes by default.
                if not chunk:
                    break
                size += len(chunk)
                f.write(chunk)
                sha1.update(chunk)

        # Write check info
        upload.size = size
        upload.sha1 = sha1.hexdigest()

        # Analyze color
        upload.color = Upload.detect_color(datetime.datetime.now())

        # Check by checksum
        match = Upload.select().where(Upload.user == user,
                                      Upload.sha1 == upload.sha1,
                                      Upload.size == size,
                                      Upload.deleted == False)
        if len(match.execute()):
            upload = match[0]
            os.remove(os.path.join(full_path, upload_filename))
        else:
            # Save to DB
            upload.save()

        return upload.deserialize()
