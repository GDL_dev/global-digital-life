import os
import subprocess
import tempfile
import typing

import ffmpy
from PIL import Image, ImageColor


def get_videos_thumbnail(files: typing.List[str]) -> typing.List[Image.Image]:
    """
    Get thumbnails for list of video files

    :param files: typing.List[str]
    :return: typing.List[Image.Image]
    """
    inputs, outputs = {}, {}
    results = []

    for file in files:
        temp = tempfile.mktemp('.png', 'thumbnail_')
        inputs[file] = None
        outputs[temp] = '-ss 00:00:00 -vframes 1'

    ffmpy.FFmpeg(
        global_options=['-y'],
        inputs=inputs,
        outputs=outputs
    ).run(stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    for file in outputs.keys():
        results.append(Image.open(file))
        os.unlink(file)
    return results


def create_box(photo: Image.Image, size: int, color: str, spacer: int = 5) -> Image.Image:
    """
    Create colored square and scale photo in center

    :param photo: Photo
    :param size: Square size
    :param color: Background color
    :param spacer: Border width
    :return: Square
    """
    # Create square with background color
    result_img = Image.new('RGB', (size, size), ImageColor.getrgb(color))

    # Resize photo
    size = size - spacer * 2
    original_width, original_height = photo.size
    if original_width > size:
        scale = size / photo.width
        new_size = (size, int(original_height * scale))
    else:
        scale = size / photo.height
        new_size = (int(original_width * scale), size)
    image = photo.resize(new_size, Image.ANTIALIAS)

    # Paste photo into square
    result_img.paste(image, (int(size / 2 - image.width / 2) + spacer,
                             int(size / 2 - image.height / 2) + spacer))

    return result_img
