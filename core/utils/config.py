import glob
import json
import os

import yaml

JSON_MODE = ['json']
YAML_MODE = ['yml', 'yaml']
MODES = JSON_MODE + YAML_MODE


class Config:
    def __init__(self, config, filename=None, mode=None, read_only=None):
        if filename is None:
            read_only = False

        self.filename = filename
        self.mode = mode

        self.__config = config
        self.__read_only = read_only

    @property
    def data(self):
        return self.__config.copy()

    def is_read_only(self):
        return self.__read_only

    def reconfigure(self, filename, mode=None, read_only=None, update=None):
        if update is None:
            update = {}

        if self.filename == filename:
            raise NameError('New filename is required!')

        if mode is True:
            mode = detect_mode(filename)

        self.filename = filename
        self.mode = mode or self.mode
        self.__read_only = read_only or self.is_read_only()
        self.__config.update(update)

    def __getitem__(self, item):
        return self.__config[item]

    def __setitem__(self, key, value):
        if self.__read_only:
            raise RuntimeError('Config is locked!')

        self.__config[key] = value

    def __delitem__(self, key):
        if self.__read_only:
            raise RuntimeError('Config is locked!')

        del self.__config[key]

    def get(self, key, default=None):
        return self.__config.get(key, default)

    def update(self, iterable: dict = None, **kwargs):
        if iterable:
            self.__config.update(iterable)
        self.__config.update(kwargs)

    def save(self, filename=None, mode=None, **kwargs):
        if self.__read_only:
            raise RuntimeError('Config is locked!')

        if filename and mode is None:
            mode = detect_mode(filename)

        _mode = mode or self.mode or 'json'
        _filename = filename or self.filename

        if _mode not in MODES:
            raise RuntimeError('Unknown mode')

        with open(_filename, 'w') as file:
            if _mode in JSON_MODE:
                json.dump(self.__config, file, **kwargs)
            elif _mode in YAML_MODE:
                yaml.dump(self.__config, file, **kwargs)

    @classmethod
    def from_file(cls, filename, mode=None, read_only=True):
        if not os.path.isfile(filename):
            raise FileNotFoundError(filename)

        if mode is None:
            mode = detect_mode(filename)

        if mode not in MODES:
            raise RuntimeError('Unknown mode')

        with open(filename, 'r') as file:
            if mode in JSON_MODE:
                data = json.load(file)
            elif mode in YAML_MODE:
                data = yaml.load(file)
            else:
                data = None

        return Config(data, filename=filename, mode=mode, read_only=read_only)

    @classmethod
    def from_json(cls, filename, read_only=True):
        return Config.from_file(filename, mode='json', read_only=read_only)

    @classmethod
    def from_yaml(cls, filename, read_only=True):
        return Config.from_file(filename, mode='yaml', read_only=read_only)


def detect_mode(filename):
    mode = None

    _, __, ext = filename.lower().rpartition('.')
    if ext:
        if ext in JSON_MODE:
            mode = 'json'
        elif ext in YAML_MODE:
            mode = 'yaml'

    return mode


def find_config(path, filename):
    return glob.glob(os.path.join(path, filename + '.*'))[0]
