import sys

import aiohttp
import jinja2
import peewee
import psycopg2

from . import __version__


def print_versions():
    print(get_versions())


def get_versions():
    return f"Python {sys.version} on {sys.platform}\n" \
           f"GDL Backend server {__version__}\n" \
           f"aiohttp {aiohttp.__version__}\n" \
           f"peewee {peewee.__version__}\n" \
           f"psycopg2 {psycopg2.__version__}\n" \
           f"jinja2 {jinja2.__version__}"
