import logging.config
import os
import time

import yaml
from aiohttp import web

import core
import settings


def set_timezone():
    timezone = settings.TIMEZONE
    os.environ['TZ'] = timezone
    time.tzset()


def setup_logging():
    logging_config = os.environ.get('LOGGING', None)
    if logging_config:
        path = os.path.abspath(logging_config)
    else:
        path = os.path.join(core.BASE_DIR, 'logging.yaml')

    with open(path, 'r') as file:
        config = yaml.load(file)
    logging.config.dictConfig(config)


def start_web_server(**kwargs):
    web.run_app(core.app, loop=core.loop, **kwargs)
