import logging
import os
from importlib import import_module, reload

import core

log = logging.getLogger('scripts')

loaded = []


def load_module(package: str, name: str):
    path = os.path.join(core.BASE_DIR, 'scripts', *name.split('.')) + '.py'
    module_name = package + '.' + name
    if not os.path.exists(path):
        log.error(f"Script {name} is not found!")
        exit()

    log.info(f"Load script '{name}' form '{path}'")
    try:
        module = import_module(module_name)
        if module_name in loaded:
            module = reload(module)
    except Exception as e:
        log.exception(e)
    finally:
        loaded.append(module_name)


def load_script(name: str):
    return load_module('scripts', name)
