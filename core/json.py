import ujson


def dumps(data, **kwargs):
    return ujson.dumps(data, ensure_ascii=False, indent=4, **kwargs)


def loads(data, **kwargs):
    return ujson.loads(data, **kwargs)


class Deserializable:
    public_fields = []

    def deserialize(self, *, include=None, exclude=None):
        include = include or []
        exclude = exclude or []

        result = {}

        for key in self.public_fields + include:
            if key not in result and key not in exclude:
                if key.startswith('~'):
                    key = key.lstrip('~')
                    value = getattr(self, 'deserialize_' + key)()
                else:
                    value = getattr(self, key)
                if value is None:
                    continue
                result[key] = value
        return result
