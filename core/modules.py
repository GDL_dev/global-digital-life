import logging
from importlib import import_module

from core.utils.imports import list_submodules

log = logging.getLogger('ModulesLoader')


class ModulesLoader:
    def __init__(self):
        self.modules = {}
        self.objects = []

    def load_module(self, name):
        log.debug(f"Import {name}")
        if name in self.modules:
            return False

        module_ = import_module(name)

        for obj_name in dir(module_):
            obj = getattr(module_, obj_name)
            if isinstance(obj, type) and obj not in self.objects:
                self.objects.append(obj)

        if hasattr(module_, '__path__'):
            return [self.load_module(name + '.' + submodule) for submodule in list_submodules(name)]

        return True
