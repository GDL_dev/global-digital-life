import datetime
import logging

from peewee import Entity
from playhouse.migrate import PostgresqlMigrator, operation, SQL, Clause
from playhouse.postgres_ext import DateTimeTZField, Model, TextField

from core import db, modules

log = logging.getLogger('DB')


def _db_table_func(model: object):
    if hasattr(model, 'table_name'):
        name = getattr(model, 'table_name')
        if isinstance(name, str):
            return name

    name = ''.join(('_' + symbol if symbol.isupper() and pos > 1 else symbol
                    for pos, symbol in enumerate(model.__name__))).lower()
    if not name.endswith('s'):
        name += 's'
    return name


class BaseModel(Model):
    table_name = None

    created_date = DateTimeTZField(default=datetime.datetime.now, null=True)
    modified_date = DateTimeTZField(default=None, null=True)

    @property
    def pk(self):
        return self.get_id()

    def save(self, force_insert=False, only=None):
        if self.created_date is None:
            date = datetime.datetime.now()
            self.created_date = date
            self.modified_date = date
        else:
            self.modified_date = datetime.datetime.now()
        super(BaseModel, self).save(force_insert, only)

    class Meta:
        database = db
        db_table_func = _db_table_func


class CITextField(TextField):
    db_field = 'citext'


def create_tables():
    models = filter(lambda item: issubclass(item, (Model, BaseModel)) and item not in (Model, BaseModel),
                    modules.objects)
    available_tables = db.get_tables()
    need_create = set()
    for model in models:
        table_name = model._meta.db_table
        if table_name not in available_tables:
            need_create.add(model)

    if need_create:
        log.info('Create tables: ' + ', '.join([table._meta.db_table for table in need_create]))
        db.create_tables(need_create, True)


class CustomPostgresqlMigrator(PostgresqlMigrator):
    @operation
    def change_column_type(self, table, column, new_type):
        return Clause(
            *self._alter_column(table, column),
            SQL('SET DATA TYPE'),
            SQL(new_type),
            SQL('USING'),
            Entity(column)
        )
