import asyncio
import logging
import os
import signal
import sys

from aiohttp import web

log = logging.getLogger('FileWatcher')


class TouchReloadWatcher:
    def __init__(self, files, timeout=5, sleep=2, loop=None, restart=True):
        self.files = {}
        self.timeout = timeout
        self.sleep = sleep
        self.loop = loop or asyncio.get_event_loop()
        self._watch = False
        self.restart = restart

        self.handlers = []

        [self.add_file(file) for file in files]

    def register_handler(self, callback, index=0):
        self.handlers.insert(index, callback)

    def add_file(self, filename):
        self.files[filename] = os.path.getmtime(filename)

    def configure_app(self, app: web.Application):
        app.on_startup.append(self._on_startup)
        app.on_shutdown.append(self._on_shutdown)
        self.register_handler(self.on_changed)

    async def _on_startup(self, app):
        if self._watch:
            return False
        self._watch = True
        self.loop.create_task(self._listener())

    async def _on_shutdown(self, app):
        self.stop()

    async def _listener(self):
        while self._watch:
            await self._check_files()
            await asyncio.sleep(self.timeout)

    async def _check_files(self):
        for filename, mtime in self.files.items():
            current_mtime = os.path.getmtime(filename)
            if current_mtime != mtime:
                return await self._trigger_changed(filename)

    async def _trigger_changed(self, filename):
        log.warning("'{0}' is changed!".format(filename))
        await asyncio.sleep(self.sleep)
        for callback in self.handlers:
            await callback(filename)

    def stop(self):
        self._watch = False

    async def on_changed(self, filename):
        os.kill(os.getpid(), signal.SIGINT)
        if self.restart:
            os.execv(sys.executable, [sys.executable] + sys.argv)
