from playhouse.migrate import migrate

import core
from core.database import CustomPostgresqlMigrator, CITextField
from modules.uploads.models import Upload

migrator = CustomPostgresqlMigrator(core.db)

color = CITextField(null=True, default=None)

migrate(
    migrator.add_column('uploads', 'color', color)
)

uploads = Upload.select().where(Upload.color == None)

for upload in uploads:
    upload.color = Upload.detect_color(upload.created_date)
    upload.save()
