## Внешние зависимости проекта:
 - Python >= 3.6
 - PostgreSQL

 Note: Под Windows могут быть проблемы с запуском, поскольку некоторые операции asyncio не поддерживаются ОС.

## Установка
Установка зависимостей `pip install -r requirements.txt`

## Подготовка базы данных:
 - Создание пользователя: `createuser -DRSP test_user`
 - Создание БД для пользователя `createdb -O test_user test_db`
 - Вход в pSQL shell: `psql test_db`
 - Добавление поддержки hstore: `CREATE EXTENSION hstore;`
 - Добавление поддержки citext: `CREATE_EXTENSION citext;`

## Запуск:
 - `python main.py --runserver --port 8082`
 - Для запуска дополнительных скриптов и для выполнения миграций использовать параметр: `-s <name>`

## Для перезапуска на сервере Illemius:
Перейти в директорию проекта и выполнить комнаду `touch me`
